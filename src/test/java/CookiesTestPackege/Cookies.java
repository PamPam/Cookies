package CookiesTestPackege;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.testng.Assert;

public class Cookies {

    @FindBy(how = How.XPATH, using = "//H1[@class='text-center text-primary'][text()='The gibberish talking cookie']")
    private static WebElement Title;
    @FindBy(how = How.ID, using = "set-cookie")
    private static WebElement SetCookie;
    @FindBy(how = How.ID, using = "delete-cookie")
    private static WebElement Delete;
    @FindBy(how = How.XPATH, using = "//H1[@id='cookie-value']")
    private static WebElement CookieValue;

    public void pressDel(){
        Assert.assertTrue(Delete.isDisplayed());
        Delete.click();
    }
    public String takeCookie(){
        Assert.assertTrue(SetCookie.isDisplayed());
        Assert.assertTrue(Delete.isDisplayed());
        String CookieVal = CookieValue.getText();
        return CookieVal;
    }
    public void pressSet(){
        Assert.assertTrue(SetCookie.isDisplayed());
        SetCookie.click();
    }

}
