package CookiesTestPackege;

import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.util.Set;

public class TestCookies extends Base{


    @Test
    public static void TestSet(){
        driver.navigate().to("http://ec2-35-159-5-67.eu-central-1.compute.amazonaws.com/cookie.html");
        driver.manage().window().maximize();
        Cookies Test1 = PageFactory.initElements(driver, Cookies.class);
        Test1.pressSet();
        Set<Cookie> cookiesV = driver.manage().getCookies();
        System.out.println("cookie value: " + cookiesV.size() );
        for(Cookie c:cookiesV){
        System.out.println("name: " + c.getName());
        System.out.println("value: " + c.getValue());
        }
    }

    @Test
    public static void TestDelete(){

        driver.get("http://ec2-35-159-5-67.eu-central-1.compute.amazonaws.com/cookie.html");
        Cookies Test2 = PageFactory.initElements(driver, Cookies.class);
        Test2.pressDel();
        Set<Cookie> cookiesV = driver.manage().getCookies();
        System.out.println("cookie value: " + cookiesV.size() );
        if(cookiesV.size()>0){
            System.out.println("Test fail !");
        }
        else {
            System.out.println("Test succeeded !");
        }

    }


}
