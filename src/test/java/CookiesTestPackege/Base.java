package CookiesTestPackege;

import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.BeforeTest;


public class Base {

    public static WebDriver driver;
    public static void delay() {
        try {
            Thread.sleep(1000);
        }
        catch (Exception ex) {
        }
    }

    public static void CleanCkBefore() {
        driver.manage().deleteAllCookies();
    }

    public static void CreateC( String cookie, String cookieVal) {
        Cookie Cval = new Cookie(cookie, cookieVal);
        driver.manage().addCookie(Cval);
    }

    @BeforeTest

    public static  void OpenDriver(){
    System.setProperty("webdriver.chrome.driver", "src\\main\\Drivers\\chromedriver.exe");
    driver = new ChromeDriver();
    driver.manage().window().maximize();
}


}
